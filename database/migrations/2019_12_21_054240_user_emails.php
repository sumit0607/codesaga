<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_emails', function (Blueprint $table) {
            $table->Increments('id');
            $table->Integer('user_id')->default(0);
            $table->string('email_address',100);
            $table->tinyInteger('is_default');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        }); //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
