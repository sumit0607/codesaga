<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('register');
});

Route::get('/login', function () {
    return view('login');
});


Route::post('/register_user','Auth\RegisterController@store_user');

Route::post('/loginuser','Auth\LoginController@login');

Route::middleware(['auth'])->group(function () {

		Route::post('/createPlylist','PlaylistController@create');
		Route::get('/viewDashboard','PlaylistController@viewDashboard' );
		Route::get('/deletePlaylist/{id}','PlaylistController@deletePlaylist' );
		Route::get('/editPlaylist/{id}','PlaylistController@editPlaylist' );
		Route::get('/logout','PlaylistController@logout' );
		Route::post('/updateDefaultEmail','PlaylistController@updateDefaultEmail');
});