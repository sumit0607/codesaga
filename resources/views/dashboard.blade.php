<!-- Stored in resources/views/login.blade.php -->

@extends('layout.registerform')


@section('content')
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title">Dashboard</h2>
                   
                        <h3 style="text-align: right;"><a href="{{ URL::to('/logout/' ) }}">Logout</a></h3>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                     <label class="label">User Email List</label>
                                    <ul>
                                        <form action="/updateDefaultEmail"  method="post">
                                            @csrf
                                        @foreach ($emailList as $val)
                                            <li>{{ $val->email_address }}   <input type="radio" name="userEmail" value="{{$val->id}}"  {{ (isset($emailList) && $val->is_default==1)?'checked':'' }}></li>

                                        @endforeach
                                         <div class="p-t-15">
                                 <button class="btn btn--radius-2 btn--blue" type="submit">Submit</button>
                                </div>
                                        </form>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                       
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                     <label class="label">Playlist</label>
                                    <ul>
                                        @foreach ($playList as $val)
                                            <li>{{ $val->title }}   <a href="{{ URL::to('/editPlaylist/' ) }}{{'/'.$val->id}}">Edit</a>  <a href="{{ URL::to('/deletePlaylist/') }}{{'/'.$val->id}}">Delete</a></li>
                                        @endforeach
                                        
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row row-space">
                        <form action="/createPlylist" method="post">
                            <label class="label">Create Playlist</label>
                            <div class="col-2">
                                @csrf
                                <div class="input-group">
                                    <label class="label">Playlist Name</label>
                                    <input class="input--style-4" type="text" name="playlist_name" value="{{ ($playListData->title)??'' }}" required>
                                </div>
                                <input type="hidden" name="id" value="{{  (isset($playListData) && $playListData->id)?$playListData->id:'' }}">
                               
                                 <div class="input-group">
                                    <label class="label">Playlist Genre</label>
                                   <select name="genre">
                                     <option value="1" {{ (isset($playListData) && $playListData->genre==1)?'selected':'' }}>Rock</option>
                                     <option value="2" {{ (isset($playListData) && $playListData->genre==2)?'selected':'' }}>Opera</option>
                                     <option value="3" {{ (isset($playListData) && $playListData->genre==3)?'selected':'' }}>World</option>
                                     <option value="4" {{ (isset($playListData) && $playListData->genre==4)?'selected':'' }}>Pop</option>
                                     <option value="5" {{ (isset($playListData) && $playListData->genre==5)?'selected':'' }}>Electronic</option>
                                     <option value="6" {{ (isset($playListData) && $playListData->genre==6)?'selected':'' }}>Classical</option>
                                   </select>
                                </div>
                                <div class="input-group">
                                    <label class="label">Playlist Mood</label>
                                   <select name="mood">
                                     <option value="1" {{ (isset($playListData) && $playListData->mood==1)?'selected':'' }}>Sad</option>
                                     <option value="2" {{ (isset($playListData) && $playListData->mood==2)?'selected':'' }}>Party</option>
                                     <option value="3" {{ (isset($playListData) && $playListData->mood==3)?'selected':'' }}>Motivated</option>
                                     <option value="4" {{ (isset($playListData) && $playListData->mood==4)?'selected':'' }}>Calm</option>
                                     <option value="5" {{ (isset($playListData) && $playListData->mood==5)?'selected':'' }}>Romantic</option>
                                   </select>
                                </div>
                            </div>
                            <div class="p-t-15">
                                 <button class="btn btn--radius-2 btn--blue" type="submit">Submit</button>
                             </div>
                        </form>
                            
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection