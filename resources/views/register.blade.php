<!-- Stored in resources/views/login.blade.php -->

@extends('layout.registerform')


@section('content')
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title">Registration Form</h2>

                    <div>
                    <ul>
                    @foreach($errors->all() as $i)

                    <li>{{$i}}</li>

                    @endforeach
                    
                    </ul>
                    
                    
                    </div>
                    <form action="/register_user" method="POST">
                    	@csrf
                        
                        <div class="row row-space">
                     
                            <div class="col-2">
                        
                                <div class="input-group">
                                
                                    <label class="label">first name</label>
                                    <input class="input--style-4" type="text" name="first_name" >
                                    <p class="help is-danger">{{$errors->first("first_name")}}</p>
                                </div>
                                
                            
                            
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">last name</label>
                                    <input class="input--style-4" type="text" name="last_name" >
                                    @error("last_name")
                                    <div>{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            
                        </div>
              
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email" name="email" >
                                    @error("email")
                                    <div>{{$message}}</div>
                                    @enderror

                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email-2</label>
                                    <input class="input--style-4" type="email" name="email-2" >
                                    @error("email-2")
                                    <div>{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Password</label>
                                    <input class="input--style-4" type="password" name="password" >
                                    @error("password")
                                    <div>{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            
                        </div>
                       
                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit">Submit</button>
                        </div>
                        
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection