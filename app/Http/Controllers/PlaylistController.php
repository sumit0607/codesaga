<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Routing\UrlGenerator;

class PlaylistController extends Controller
{ 
    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function create(Request $request){
    	$this->checkSession();
    	$data = $request->all();
    	$userId= session('user_id'); 
    	$validatedData = $request->validate([
            'playlist_name' => 'required|max:255',
            'genre' => 'required|max:255',
            'mood' => 'required|max:255',
          ]);
    	if($request->input('id')!=''){
    		DB::table('Playlists')
            ->where('id', $request->id)
            ->update(['title' => $request->input('playlist_name') , 'genre'=> $request->input('genre'), 'mood'=>$request->input('mood') , 'updated_at'=>date('Y-m-d H:i:s')]);
    	}else{
	    	DB::table('Playlists')->insert(
	    		['user_id' => $userId, 'title' => $request->input('playlist_name') , 'genre'=> $request->input('genre'), 'mood'=>$request->input('mood') , 'created_at'=>date('Y-m-d H:i:s') , 'updated_at'=>date('Y-m-d H:i:s'), 'is_delete'=>0]
			);
    	}
		return redirect('/viewDashboard');

    }

    public function viewDashboard(){
    	$this->checkSession();
    	$userId= session('user_id'); 
    	$playList = DB::table('Playlists')->where('user_id',$userId)->where('is_delete',0)->get();
    	$emailList = DB::table('user_emails')->where('user_id',$userId)->get();
    	return view('dashboard',compact('playList','emailList'));
    }

    public function deletePlaylist($id){
    	$this->checkSession();
    	DB::table('Playlists')
            ->where('id', $id)
            ->update(['is_delete'=>1]);
    	return redirect('/viewDashboard');
    }

    public function editPlaylist($id){
    	$this->checkSession();
    	$userId= session('user_id'); 
    	$playList = DB::table('Playlists')->where('user_id',$userId)->where('is_delete',0)->get();
    	$playListData = DB::table('Playlists')->where('id',$id)->where('is_delete',0)->first();
    	$emailList = DB::table('user_emails')->where('user_id',$userId)->get();
    	return view('dashboard',compact('playList','playListData','emailList'));
    }

    public function logout(){
    	session(['user_id' => null]);
    	$user = Auth::user();

        if (! empty($user->logout)) {
            
            $user->logout = false;
            $user->save();

        
            Auth::logout();

            return redirect()->route('/login');
        }
    	return redirect('/login');	
    }

    public function checkSession(){
    	$x = session('user_id');
    	if(is_null($x) || $x == 'NULL' || empty($x)){ 
    		header("Location: ".$this->url->to('/login'));
    		exit();
    	}
    }

    public function updateDefaultEmail(Request $request){
    	$userId = session('user_id');
    	$id = $request->input("userEmail");

    	DB::table('user_emails')
            ->where('user_id', $userId)
            ->update(['is_default'=>0]);
    	
    	DB::table('user_emails')
            ->where('id', $id)
            ->update(['is_default'=>1]);

         return redirect('/viewDashboard');
    }
}