<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'password' => Hash::make($data['password']),
        ]);
    }

 
   public function store_user(Request $request){

     $validatedData = $request->validate([
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'email' => 'required|max:255',"unique:users",
        'email-2' => 'required|max:255',"unique:users",
        'password' => 'required|min:8', "confirmed",
    ]);

    $sendAr = array(
        "first_name"=>$request->input('first_name'),
        "last_name"=>$request->input('last_name'),
        "password"=>$request->input('password')
     );

    $emailVal = DB::table('user_emails')->where('email_address', $request->input('email'))->orWhere('email_address', $request->input('email-2'))->get();
        
      if(count($emailVal)==0){
        $userRegister = $this->create($sendAr);

        DB::table('user_emails')->insert([
            ['email_address' => $request->input('email'), 'user_id' => $userRegister->id,'is_default'=>1,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
            ['email_address' => $request->input('email-2'), 'user_id' => $userRegister->id,'is_default'=>0,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        ]);
         return redirect('/login');
    }else{
       echo "<script>";
       echo "alert('Email already exist')";
       echo "</script>";
       
     return redirect('/');
    }

   }
}
