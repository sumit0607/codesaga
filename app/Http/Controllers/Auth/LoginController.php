<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/viewDashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $login){

          $validatedData = $login->validate([
            'email' => 'required|max:255',
            'password' => 'required|max:255',
          ]);
          
        $email = $login->input('email');
        $query = "SELECT users.* FROM `users` JOIN user_emails ON users.id = user_emails.user_id where  user_emails.email_address ='$email' AND user_emails.is_default='1'";

        $results = DB::select( DB::raw($query) );
        
        if(count($results)>0 && Hash::check($login->input('password'), $results[0]->password)){
        
            Auth::loginUsingId($results[0]->id, TRUE);
            $user = Auth::user();
            session(['user_id' => $results[0]->id]);
            return redirect('/viewDashboard');
        }else{
            

            return redirect('/login');
            
           
        }
        
    }


    
}
